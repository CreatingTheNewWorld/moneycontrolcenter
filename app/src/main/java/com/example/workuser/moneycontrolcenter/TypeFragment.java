package com.example.workuser.moneycontrolcenter;


import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.workuser.moneycontrolcenter.adapter.ImageAdapter;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class TypeFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView1_1;
    private ImageView imageView1_2;
    private ImageView imageView1_3;
    private ImageView imageView1_4;
    private ImageView imageView1_5;

    private ImageView imageView2_1;
    private ImageView imageView2_2;
    private ImageView imageView2_3;
    private ImageView imageView2_4;
    private ImageView imageView2_5;

    private ImageView imageView3_1;
    private ImageView imageView3_2;
    private ImageView imageView3_3;
    private ImageView imageView3_4;
    private ImageView imageView3_5;

    TypeListener typeListener;

    String chosenType = null;

    Map<String,ImageView> buttonsMap = new HashMap();



    public TypeFragment() {
        // Required empty public constructor
    }


    interface TypeListener{

        void pressedType(String type, View v);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        typeListener = (TypeListener) context;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_type, container, false);

        setImageViews(v);

        setListeners();

        setButtonsMap();

        return v;
    }

    private void setListeners() {

        imageView1_1.setOnClickListener(this);
        imageView1_2.setOnClickListener(this);
        imageView1_3.setOnClickListener(this);
        imageView1_4.setOnClickListener(this);
        imageView1_5.setOnClickListener(this);

        imageView2_1.setOnClickListener(this);
        imageView2_2.setOnClickListener(this);
        imageView2_3.setOnClickListener(this);
        imageView2_4.setOnClickListener(this);
        imageView2_5.setOnClickListener(this);

        imageView3_1.setOnClickListener(this);
        imageView3_2.setOnClickListener(this);
        imageView3_3.setOnClickListener(this);
        imageView3_4.setOnClickListener(this);
        imageView3_5.setOnClickListener(this);
    }

    private void setImageViews(View v) {

        imageView1_1  =   v.findViewById(R.id.imageView1_row1_fragmenttype);
        imageView1_2  =   v.findViewById(R.id.imageView2_row1_fragmenttype);
        imageView1_3  =   v.findViewById(R.id.imageView3_row1_fragmenttype);
        imageView1_4  =   v.findViewById(R.id.imageView4_row1_fragmenttype);
        imageView1_5  =   v.findViewById(R.id.imageView5_row1_fragmenttype);

        imageView2_1  =   v.findViewById(R.id.imageView1_row2_fragmenttype);
        imageView2_2  =   v.findViewById(R.id.imageView2_row2_fragmenttype);
        imageView2_3  =   v.findViewById(R.id.imageView3_row2_fragmenttype);
        imageView2_4  =   v.findViewById(R.id.imageView4_row2_fragmenttype);
        imageView2_5  =   v.findViewById(R.id.imageView5_row2_fragmenttype);

        imageView3_1  =   v.findViewById(R.id.imageView1_row3_fragmenttype);
        imageView3_2  =   v.findViewById(R.id.imageView2_row3_fragmenttype);
        imageView3_3  =   v.findViewById(R.id.imageView3_row3_fragmenttype);
        imageView3_4  =   v.findViewById(R.id.imageView4_row3_fragmenttype);
        imageView3_5  =   v.findViewById(R.id.imageView5_row3_fragmenttype);

    }

    private void setButtonsMap() {

        buttonsMap.put("products",imageView1_1);
        buttonsMap.put("clothes",imageView1_2);
        buttonsMap.put("rest",imageView1_3);
        buttonsMap.put("soccer",imageView1_4);
        buttonsMap.put("repairs",imageView1_5);
        buttonsMap.put("bill",imageView2_1);
        buttonsMap.put("sport",imageView2_2);
        buttonsMap.put("dating",imageView2_3);
        buttonsMap.put("telephone",imageView2_4);
        buttonsMap.put("petrol",imageView2_5);
        buttonsMap.put("gifts",imageView3_1);
        buttonsMap.put("house",imageView3_2);
        buttonsMap.put("doctor",imageView3_3);
        buttonsMap.put("internet",imageView3_4);
        buttonsMap.put("restaurant",imageView3_5);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imageView1_row1_fragmenttype:{
                chosenType(imageView1_1,"products");
                break;
            }
            case R.id.imageView2_row1_fragmenttype:{
                chosenType(imageView1_2,"clothes");
                break;
            }
            case R.id.imageView3_row1_fragmenttype:{
                chosenType(imageView1_3,"rest");
                break;
            }
            case R.id.imageView4_row1_fragmenttype:{
                chosenType(imageView1_4,"soccer");
                break;
            }
            case R.id.imageView5_row1_fragmenttype:{
                chosenType(imageView1_5,"repairs");
                break;
            }
            case R.id.imageView1_row2_fragmenttype:{
                chosenType(imageView2_1,"bill");
                break;
            }
            case R.id.imageView2_row2_fragmenttype:{
                chosenType(imageView2_2,"sport");
                break;
            }
            case R.id.imageView3_row2_fragmenttype:{
                chosenType(imageView2_3,"dating");
                break;
            }
            case R.id.imageView4_row2_fragmenttype:{
                chosenType(imageView2_4,"telephone");
                break;
            }
            case R.id.imageView5_row2_fragmenttype:{
                chosenType(imageView2_5,"petrol");
                break;
            }
            case R.id.imageView1_row3_fragmenttype:{
                chosenType(imageView3_1,"gifts");
                break;
            }
            case R.id.imageView2_row3_fragmenttype:{
                chosenType(imageView3_2,"house");
                break;
            }
            case R.id.imageView3_row3_fragmenttype:{
                chosenType(imageView3_3,"doctor");
                break;
            }
            case R.id.imageView4_row3_fragmenttype:{
                chosenType(imageView3_4,"internet");
                break;
            }
            case R.id.imageView5_row3_fragmenttype:{
                chosenType(imageView3_5,"restaurant");
                break;
            }
        }
    }

    private void chosenType(View v, String type) {

        ColorDrawable oldButtonColor = null;

        ColorDrawable buttonColor = (ColorDrawable) v.getBackground();
        int colorId = buttonColor.getColor();

        if (chosenType == null){
            v.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            chosenType = type;

        }else if (chosenType == type ){

            if (colorId == -2298424){
                v.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                chosenType = type;
            }else{
                v.setBackgroundColor(getResources().getColor(R.color.colorLightGreen100));
                chosenType = null;
            }
        }else{

            ImageView oldBtn = getButton(chosenType);
            if (oldBtn == null) {
                Toast.makeText(getActivity(), "No exist " + chosenType + "!" , Toast.LENGTH_SHORT).show();
                chosenType = null;
            }else{

                oldBtn.setBackgroundColor(getResources().getColor(R.color.colorLightGreen100));
                v.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                chosenType = type;
            }
        }
        typeListener.pressedType(chosenType,v);
    }

    public ImageView getButton(String key) {

        return buttonsMap.get(key);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        buttonsMap.clear();
    }
}


