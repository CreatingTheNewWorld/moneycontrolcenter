package com.example.workuser.moneycontrolcenter.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.workuser.moneycontrolcenter.MainActivity;
import com.example.workuser.moneycontrolcenter.R;

import static com.example.workuser.moneycontrolcenter.MainActivity.EXPENSE;
import static com.example.workuser.moneycontrolcenter.MainActivity.INCOME;
import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;

public class BalanceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "BalanceActivity";

    private ImageButton mImageButtonAddIncome;
    private ImageButton mImageButtonJournal;
    private ImageButton mImageButtonBalance;
    private ImageButton mImageButtonMoneyBox;
    private ImageButton mImageButtonMoneyGraph;

    private TextView mTvBalance;
    private TextView mTvIncome;
    private TextView mTvExpense;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mImageButtonAddIncome = findViewById(R.id.iB_addIncome_activity_main);
        mImageButtonJournal = findViewById(R.id.iB_journal_activity_main);
        mImageButtonBalance = findViewById(R.id.iB_balance_activity_main);
        mImageButtonMoneyBox = findViewById(R.id.iB_moneyBox_activity_main);
        mImageButtonMoneyGraph = findViewById(R.id.iB_graph_activity_main);

        mTvBalance = findViewById(R.id.tv_balance_activity_balance);
        mTvExpense = findViewById(R.id.tv_expense_activity_balance);
        mTvIncome = findViewById(R.id.tv_income_activity_balance);


        mImageButtonAddIncome.setOnClickListener(this);
        mImageButtonJournal.setOnClickListener(this);
        mImageButtonBalance.setOnClickListener(this);
        mImageButtonMoneyBox.setOnClickListener(this);
        mImageButtonMoneyGraph.setOnClickListener(this);



        String colums[] = {"operation", "sum"};

        //get balance
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI_MONEY, colums, null, null, null);

        double income = 0;
        double expenses = 0;
        double balance = 0;

        if (cursor.moveToFirst()) {
            do {
                // TODO: 27.01.2018 replace request to service
                int operation = cursor.getInt(cursor.getColumnIndex("operation"));

                double sum = cursor.getDouble(cursor.getColumnIndex("sum"));

                if (operation == EXPENSE){
                    expenses = expenses + sum;
                }else if(operation == INCOME){
                    income = income  + sum;
                }

            } while (cursor.moveToNext());


            balance = income - expenses;
        }
        cursor.close();


        mTvBalance.setText(String.valueOf(balance));
        mTvIncome.setText(String.valueOf(income));
        mTvExpense.setText(String.valueOf(expenses));
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        Intent startActivityIntent = null;

        switch (v.getId()) {

            case R.id.iB_addIncome_activity_main: {
                startActivityIntent = new Intent(this, MainActivity.class);
                break;
            }
            case R.id.iB_journal_activity_main: {
                startActivityIntent = new Intent(this, JournalActivity.class);
                break;
            }
            case R.id.iB_balance_activity_main: {
                break;
            }
            case R.id.iB_moneyBox_activity_main: {
                //startActivityIntent = new Intent(this,Main3Activity.class);
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.iB_graph_activity_main: {
                startActivityIntent = new Intent(this,GraphActivity.class);
                break;
            }

            default:
                break;
        }

        if (startActivityIntent != null) {
            startActivity(startActivityIntent);
        }


    }
}
