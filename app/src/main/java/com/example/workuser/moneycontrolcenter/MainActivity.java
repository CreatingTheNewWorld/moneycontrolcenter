package com.example.workuser.moneycontrolcenter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.workuser.moneycontrolcenter.activity.BalanceActivity;
import com.example.workuser.moneycontrolcenter.activity.GraphActivity;
import com.example.workuser.moneycontrolcenter.activity.JournalActivity;
import com.example.workuser.moneycontrolcenter.adapter.ImageAdapter;
import com.example.workuser.moneycontrolcenter.adapter.TypeAdapter;
import com.example.workuser.moneycontrolcenter.receiver.MoneyReceiver;
import com.example.workuser.moneycontrolcenter.service.MoneyService;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.support.v4.view.ViewCompat.setBackgroundTintList;
import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;

public class MainActivity extends AppCompatActivity implements OnClickListener, TypeFragment.TypeListener {

    private static final String TAG = "MainActivity";

    private ImageButton mImageButtonAddIncome;
    private ImageButton mImageButtonJournal;
    private ImageButton mImageButtonBalance;
    private ImageButton mImageButtonMoneyBox;
    private ImageButton mImageButtonMoneyGraph;

    //initialize calculator's btns and textView
    private TextView mTotal;
    private TextView mComment;

    private Button mBtnCalc1;
    private Button mBtnCalc2;
    private Button mBtnCalc3;
    private Button mBtnCalc4;
    private Button mBtnCalc5;
    private Button mBtnCalc6;
    private Button mBtnCalc7;
    private Button mBtnCalc8;
    private Button mBtnCalc9;
    private Button mBtnCalc0;
    private Button mBtnCalcPnt;
    private Button mBtnCalcOk;
    private Button mBtnCalcCE;
    private Button mBtnCalcCancel;
    private Button mBtnCalcComment;
    // TODO: 19.01.2018 delete  Q!
    private Button mBtnQ;

    private ImageButton mBtnIncome;
    private ImageButton mBtnExpense;

    public static final int EXPENSE = -1;
    public static final int INCOME = 1;
    private int currentOperation = -1;

    //private MoneyReceiver mMoneyBroadcastReceiver;
    private MoneyReceiver broadcastReceiver = new MoneyReceiver();

    private ViewPager mViewPager;
    //initialese var for type expense or income
    private String type = null;

    private ImageView currentIv = null;

    private Animation animAlpha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setImagesView();
        setButtons();

        setListener();

        mViewPager = findViewById(R.id.viewPager_activity_add);

        animAlpha = AnimationUtils.loadAnimation(this,R.anim.alpha);

        setViewPager();

        mComment = findViewById(R.id.textView_comment_calc);

        setEtColor(EXPENSE);
        setBtnTint();
    }

    private void setBtnTint() {

        if(currentOperation == EXPENSE){
            setBackgroundTintList(mBtnExpense,getResources().getColorStateList(R.color.colorRed300));
            setBackgroundTintList(mBtnIncome,getResources().getColorStateList(R.color.colorLightGreen100));
        }else {
            setBackgroundTintList(mBtnIncome,getResources().getColorStateList(R.color.colorLightGreen300));
            setBackgroundTintList(mBtnExpense,getResources().getColorStateList(R.color.colorLightGreen100));
        }


    }

    private void setListener() {
        mBtnCalc1.setOnClickListener(this);
        mBtnCalc2.setOnClickListener(this);
        mBtnCalc3.setOnClickListener(this);
        mBtnCalc4.setOnClickListener(this);
        mBtnCalc5.setOnClickListener(this);
        mBtnCalc6.setOnClickListener(this);
        mBtnCalc7.setOnClickListener(this);
        mBtnCalc8.setOnClickListener(this);
        mBtnCalc9.setOnClickListener(this);
        mBtnCalc0.setOnClickListener(this);
        mBtnCalcPnt.setOnClickListener(this);
        mBtnCalcOk.setOnClickListener(this);
        mBtnCalcCE.setOnClickListener(this);
        mBtnCalcCancel.setOnClickListener(this);
        mBtnCalcComment.setOnClickListener(this);

        mBtnIncome.setOnClickListener(this);
        mBtnExpense.setOnClickListener(this);

        //TODO: 19.01.2018 delete  Q!
        mBtnQ.setOnClickListener(this);

        mImageButtonAddIncome.setOnClickListener(this);
        mImageButtonJournal.setOnClickListener(this);
        mImageButtonBalance.setOnClickListener(this);
        mImageButtonMoneyBox.setOnClickListener(this);
        mImageButtonMoneyGraph.setOnClickListener(this);
    }

    private void setButtons() {
        mTotal = findViewById(R.id.et_total_calc);
        mBtnCalc1 = findViewById(R.id.button1);
        mBtnCalc2 = findViewById(R.id.button2);
        mBtnCalc3 = findViewById(R.id.button3);
        mBtnCalc4 = findViewById(R.id.button4);
        mBtnCalc5 = findViewById(R.id.button5);
        mBtnCalc6 = findViewById(R.id.button6);
        mBtnCalc7 = findViewById(R.id.button7);
        mBtnCalc8 = findViewById(R.id.button8);
        mBtnCalc9 = findViewById(R.id.button9);
        mBtnCalc0 = findViewById(R.id.button0);
        mBtnCalcPnt = findViewById(R.id.button_point);
        mBtnCalcOk = findViewById(R.id.buttonOk);
        mBtnCalcCE = findViewById(R.id.buttonCE);
        mBtnCalcCancel = findViewById(R.id.buttonCancel);
        mBtnCalcComment = findViewById(R.id.button_comment);
        //TODO: 19.01.2018 delete  Q!
        mBtnQ = findViewById(R.id.button_DELETE);

        mBtnExpense = findViewById(R.id.ib_expense);
        mBtnIncome = findViewById(R.id.ib_Income);

    }

    private void setImagesView() {
        mImageButtonAddIncome = findViewById(R.id.iB_addIncome_activity_main);
        mImageButtonJournal = findViewById(R.id.iB_journal_activity_main);
        mImageButtonBalance = findViewById(R.id.iB_balance_activity_main);
        mImageButtonMoneyBox = findViewById(R.id.iB_moneyBox_activity_main);
        mImageButtonMoneyGraph = findViewById(R.id.iB_graph_activity_main);

    }

    private void setViewPager() {


        PagerAdapter typeAdapter = new TypeAdapter(getSupportFragmentManager(),3);

        mViewPager.setAdapter(typeAdapter);




    }

    @Override
    protected void onStart() {
        super.onStart();

        registerReceiver(broadcastReceiver,new IntentFilter("result"));
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_main_expense) {

                currentOperation = EXPENSE;
                setEtColor(currentOperation);
                setBtnTint();

        } else if(id == R.id.menu_main_income) {

            currentOperation = INCOME;
                setEtColor(currentOperation);
                setBtnTint();
            }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Intent startActivityIntent = null;

        String s = "";

        switch (v.getId()) {

            case R.id.iB_addIncome_activity_main: {
                break;
            }
            case R.id.iB_journal_activity_main: {
                startActivityIntent = new Intent(this, JournalActivity.class);
                break;
            }
            case R.id.iB_balance_activity_main: {
                startActivityIntent = new Intent(this,BalanceActivity.class);
                break;
            }
            case R.id.iB_moneyBox_activity_main: {
                //startActivityIntent = new Intent(this,Main3Activity.class);
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.iB_graph_activity_main: {
                startActivityIntent = new Intent(this,GraphActivity.class);
                break;
            }

            case R.id.button1: {

                mBtnCalc1.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "1";
                mTotal.setText(s);


                break;

            }
            case R.id.button2: {


                mBtnCalc2.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "2";
                mTotal.setText(s);

                break;
            }
            case R.id.button3: {


                mBtnCalc3.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "3";
                mTotal.setText(s);

                break;
            }
            case R.id.button4: {


                mBtnCalc4.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "4";
                mTotal.setText(s);

                break;
            }
            case R.id.button5: {

                mBtnCalc5.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "5";
                mTotal.setText(s);

                break;
            }
            case R.id.button6: {

                mBtnCalc6.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "6";
                mTotal.setText(s);

                break;
            }
            case R.id.button7: {

                mBtnCalc7.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "7";
                mTotal.setText(s);

                break;
            }
            case R.id.button8: {

                mBtnCalc8.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "8";
                mTotal.setText(s);

                break;
            }
            case R.id.button9: {

                mBtnCalc9.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "9";
                mTotal.setText(s);

                break;
            }
            case R.id.button0: {

                mBtnCalc0.startAnimation(animAlpha);
                s = mTotal.getText().toString() + "0";
                mTotal.setText(s);

                break;
            }
            case R.id.button_comment: {

                mBtnCalcComment.startAnimation(animAlpha);
                // TODO: 23.01.2018 write  Alert Dialog for fill calc field "comment"
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.button_point: {

                mBtnCalcPnt.startAnimation(animAlpha);
                s = mTotal.getText().toString();

                if (s.contains(".")) {
                    break;
                }

                if (!s.equals("")) {
                    mTotal.setText(s + ".");
                } else {
                    mTotal.setText("0.");
                }

                break;
            }
            case R.id.buttonCE: {

                mBtnCalcCE.startAnimation(animAlpha);
                s = mTotal.getText().toString();
                if (s.isEmpty()) {
                    break;
                } else {
                    char[] arr = s.toCharArray();
                    int indexArr = arr.length - 1;
                    char[] newArr = Arrays.copyOf(arr, indexArr);

                    s = new String(newArr);
                    mTotal.setText(s);
                }
                break;
            }
            case R.id.buttonCancel: {

                mBtnCalcCancel.startAnimation(animAlpha);
                mTotal.setText("");
                break;
            }
            case R.id.buttonOk: {

                mBtnCalcOk.startAnimation(animAlpha);
                //save input sum to DB
                //data, operationType ,type, sum, comment;
                s = mTotal.getText().toString();

                if (!s.isEmpty()) {
                    Date dateNow = new Date();
                    SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yy");

                    String date = formatForDateNow.format(dateNow);

                    double sum = getNumbers(s);

                    String comment = mComment.getText().toString();

                    if (sum != 0) {
                        if (type == null){
                            Toast.makeText(this, "choose type of action", Toast.LENGTH_LONG).show();
                            break;
                        }

                        //start intent service for build adding operation to DB
                        Intent startMoneyService = new Intent(MainActivity.this, MoneyService.class);

                        startMoneyService.putExtra("date", date);
                        startMoneyService.putExtra("currentOperation", currentOperation);
                        startMoneyService.putExtra("sum", sum);
                        startMoneyService.putExtra("comment", comment);
                        startMoneyService.putExtra("type", type);

                        startService(startMoneyService);

                        mTotal.setText("");

                        if(currentIv!=null){
                            currentIv.setBackgroundColor(getResources().getColor(R.color.colorLightGreen100));
                        }


                    } else break;


                } else {
                    Toast.makeText(this, "field \"sum\" is empty", Toast.LENGTH_SHORT).show();
                }

                break;
            }

            case R.id.button_DELETE: {

                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();

                break;
            }

            case R.id.ib_Income: {
                currentOperation = INCOME;
                setEtColor(currentOperation);
                setBtnTint();
                break;
            }

            case R.id.ib_expense: {
                currentOperation = EXPENSE;
                setEtColor(currentOperation);
                setBtnTint();
                break;
            }

            default:
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;

        }
        if (startActivityIntent != null) {
            startActivity(startActivityIntent);
        }

    }

    public double getNumbers(String numb) {
        double newNumb = 0;
        try {

           if(numb.indexOf(".") > 0){
                String [] arr = numb.split("\\.");
               if(arr[1].length() > 2){
                   Toast.makeText(getApplicationContext(),"incorrect sum " + arr[1] , Toast.LENGTH_SHORT).show();
                   return newNumb;
               }

           }

            newNumb = Double.parseDouble(numb);
        } catch (ClassCastException e) {
            e.getMessage();
            mTotal.setText("");
            return newNumb;
        }



        return newNumb;
    }

    private void setEtColor(int type) {

        if (type == EXPENSE) {
            mTotal.setBackgroundColor(getResources().getColor(R.color.colorRed300));
        } else {
            mTotal.setBackgroundColor(getResources().getColor(R.color.colorLightGreen300));
        }
    }

    @Override
    public void pressedType(String chosenType, View chosenIv) {

        type = chosenType;
        currentIv = (ImageView)chosenIv;
    }
}
