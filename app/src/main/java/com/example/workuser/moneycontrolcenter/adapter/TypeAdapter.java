package com.example.workuser.moneycontrolcenter.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.workuser.moneycontrolcenter.TypeFragment;
import com.example.workuser.moneycontrolcenter.TypeFragment2;
import com.example.workuser.moneycontrolcenter.TypeFragment3;

public class TypeAdapter extends FragmentPagerAdapter {
    private static final String TAG = "mylog";


    int allPagers;
    final int PAGE_COUNT = 3;
    private String[] tabTitles = {"TAB 1","TAB 2","TAB 3"};


    public TypeAdapter(FragmentManager fm, int allPagers) {
        super(fm);
        this.allPagers = allPagers;

        Log.d(TAG, "TypeAdapter: Constructor");

    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "TypeAdapter: getItem");

        switch(position){
            case 0:
                return new TypeFragment();

            case 1:
                return  new TypeFragment2();

            case 2:

                return new TypeFragment3();

            default: return null;
        }
    }

    @Override
    public int getCount() {
        Log.d(TAG, "TypeAdapter: getCount");

        return allPagers;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d(TAG, "TypeAdapter: getPageTitle");

        return "Title " + position+1;
    }
}
