package com.example.workuser.moneycontrolcenter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RadioGroup;

import com.example.workuser.moneycontrolcenter.adapter.ImageAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class TypeFragment2 extends Fragment {




    public TypeFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        View v = inflater.inflate(R.layout.fragment_type2, container, false);


        return v;
    }

}
