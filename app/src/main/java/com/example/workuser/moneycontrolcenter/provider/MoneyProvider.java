package com.example.workuser.moneycontrolcenter.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;


public class MoneyProvider extends ContentProvider {

    private static final String TAG = "MoneyProvider";

    private DBHelper dataBase;
    private SQLiteDatabase db;

    //URI
    public static final String CONTENT_AUTHORITY = "com.example.workuser.moneycontrolcenter.provider";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    private static final String PATH_MONEY = "money_table";
    private static final String PATH_AUTH = "auth_table";
    public static final Uri CONTENT_URI_MONEY = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MONEY).build();
    public static final Uri CONTENT_URI_AUTH = BASE_CONTENT_URI.buildUpon().appendPath(PATH_AUTH).build();

    private static final int MONEY = 1001;
    private static final int AUTH = 2001;

    private static final UriMatcher sURI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURI_MATCHER.addURI(CONTENT_AUTHORITY, PATH_MONEY, MONEY);
        sURI_MATCHER.addURI(CONTENT_AUTHORITY, PATH_AUTH, AUTH);
    }


    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate: ");
        dataBase = new DBHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        db = dataBase.getReadableDatabase();

        int uriMatcher = sURI_MATCHER.match(uri);

        Cursor cursor;

        Context context = getContext();

        switch (uriMatcher){

            case MONEY :{

                cursor = db.query(PATH_MONEY,projection,selection,selectionArgs,null,null,sortOrder);

                // TODO: 14.02.2018
                //создаем нотификацию которая скажет нам что данные изменились в БД!
                cursor.setNotificationUri(context.getContentResolver(),uri);

                return cursor;
            }
            default:
                return null;

        }

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        db = dataBase.getWritableDatabase();

        int uriMatcher = sURI_MATCHER.match(uri);
        Uri resultUri;

        Context context = getContext();

        switch (uriMatcher){

            case MONEY :{
                long insertMoney = db.insert(PATH_MONEY,null,values);

                resultUri = ContentUris.withAppendedId(uri,insertMoney);

                // TODO: 19.01.2018 write notifychange
                //context.getContentResolver().notifyChange(uri,null,false);

                return resultUri;
            }
            default:
                return null;

        }

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        db = dataBase.getWritableDatabase();
        int uriMatcher = sURI_MATCHER.match(uri);

        Context context = getContext();

        int resultDelete = -1;

        switch (uriMatcher){

            case MONEY :{

                resultDelete = db.delete(PATH_MONEY,selection,null);

                // TODO: 14.02.2018  
                //context.getContentResolver().notifyChange(uri,null,false);

                return resultDelete;
            }
            default:
                return resultDelete;

        }

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        db = dataBase.getWritableDatabase();
        int uriMatcher = sURI_MATCHER.match(uri);
        int resultUpdate = -1;

        Context context = getContext();
        switch (uriMatcher){

            case MONEY : {

                resultUpdate = db.update(PATH_MONEY,values,selection,selectionArgs);
                // TODO: 16.02.2018
                //context.getContentResolver().notifyChange(uri,null,false);
                break;
            }
           default:
               break;

        }
        return resultUpdate;
    }

    private class DBHelper extends SQLiteOpenHelper {

        private static final int DB_VERSION = 2;
        private static final String DB_NAME = "money.db";
        //create table
        private static final String MONEY_TABLE = "money_table";
        private static final String AUTH_TABLE = "auth_table";
        //fields for table money
        private static final String MONEY_ID = "_id";
        private static final String MONEY_DATE = "date";
        private static final String MONEY_OPERATION = "operation";
        private static final String MONEY_TYPE_OPERATION = "type";
        private static final String MONEY_SUM = "sum";
        private static final String MONEY_COMMENT = "comment";
        //fields for table auth
        private static final String AUTH_ID = "_id";
        private static final String AUTH_DATE = "date";
        private static final String AUTH_NAME = "name";
        private static final String AUTH_EMAIL = "email";
        private static final String AUTH_PASSWORD = "password";


        private static final String CREATE_DB_MONEY = "CREATE TABLE " + MONEY_TABLE + " (" +

                MONEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MONEY_DATE + " TEXT, " +
                MONEY_OPERATION + " INTEGER, " +
                MONEY_TYPE_OPERATION + " TEXT, " +
                MONEY_SUM + " REAL, " +
                MONEY_COMMENT + " TEXT)";

        private static final String CREATE_DB_AUTH = "CREATE TABLE " + AUTH_TABLE + " (" +

                AUTH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AUTH_DATE + " TEXT, " +
                AUTH_NAME + " TEXT, " +
                AUTH_EMAIL + " TEXT, " +
                AUTH_PASSWORD + " TEXT);";

        private static final String DROP_TABLE_MONEY = "DROP TABLE " + MONEY_TABLE;
        //private static final String DROP_TABLE_AUTH = "DROP TABLE IF EXIST " + AUTH_TABLE;

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_MONEY);
            // TODO: 22.01.2018 new auth table
            //db.execSQL(CREATE_DB_AUTH);


        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //db.execSQL(DROP_TABLE_AUTH);
            db.execSQL(DROP_TABLE_MONEY);
            onCreate(db);
        }
    }

}
