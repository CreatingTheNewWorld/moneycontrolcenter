package com.example.workuser.moneycontrolcenter.activity;

import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.example.workuser.moneycontrolcenter.MainActivity;
import com.example.workuser.moneycontrolcenter.R;
import com.example.workuser.moneycontrolcenter.dialog.DatePickerFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.example.workuser.moneycontrolcenter.MainActivity.EXPENSE;
import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEtDate;
    private EditText mEtSum;
    private EditText mEtComment;

    private Button mBtnSave;
    private Button mBtnCancel;

    private Spinner operationSpinner;
    private SpinnerAdapter spinnerAdapter;

    private static int mCardID;
    private List<String> spinnerList;

    private Spinner typeSpinner;
    private SpinnerAdapter spinnerTypeAdapter;

    List<String> typeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Intent intentArg = getIntent();

        setEditText();

        setButton();

        setTypeList();

        setListeners();

        operationSpinner = findViewById(R.id.spinner_operation_activityUpdate);

        typeSpinner = findViewById(R.id.spinner_type_activityUpdate);

        setSpinner();

        mCardID = intentArg.getIntExtra("cardId", -1);

        LoadData();

    }

    private void setTypeList() {

        typeList.add("products");
        typeList.add("clothes");
        typeList.add("rest");
        typeList.add("soccer");
        typeList.add("repairs");
        typeList.add("bill");
        typeList.add("sport");
        typeList.add("dating");
        typeList.add("telephone");
        typeList.add("petrol");
        typeList.add("gifts");
        typeList.add("house");
        typeList.add("doctor");
        typeList.add("internet");
        typeList.add("restaurant");

    }

    private void LoadData() {

        Cursor cursor = null;
        cursor = getContentResolver().query(CONTENT_URI_MONEY, null, "_id = '" + mCardID + "'", null, null);

        if (cursor.moveToFirst()) {

            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            int operation = cursor.getInt(cursor.getColumnIndex("operation"));
            String type = cursor.getString(cursor.getColumnIndex("type"));
            double sum = cursor.getDouble(cursor.getColumnIndex("sum"));
            String comment = cursor.getString(cursor.getColumnIndex("comment"));

            mEtDate.setText(date);
            mEtSum.setText(String.valueOf(sum));
            mEtComment.setText(comment);
            if(operation == EXPENSE)    operationSpinner.setSelection(1);
            else                        operationSpinner.setSelection(0);


            typeSpinner.setSelection(typeList.indexOf(type));


        }
        cursor.close();
    }

    private void setSpinner() {

        spinnerList = Arrays.asList("income", "expense");

        spinnerAdapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item,
                spinnerList);
        operationSpinner.setAdapter(spinnerAdapter);


        spinnerTypeAdapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item,
                typeList);

        typeSpinner.setAdapter(spinnerTypeAdapter);
    }

    private void setListeners() {

        mBtnSave.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);

        mEtDate.setOnClickListener(this);
        mEtSum.setOnClickListener(this);
        mEtComment.setOnClickListener(this);

    }

    private void setButton() {
        mBtnSave = findViewById(R.id.btn_save_activityUpdate);
        mBtnCancel = findViewById(R.id.btn_cancel_activityUpdate);
    }

    private void setEditText() {

        mEtDate = findViewById(R.id.et_date_activityUpdate);
        mEtSum = findViewById(R.id.et_sum_activityUpdate);
        mEtComment = findViewById(R.id.et_comment_activityUpdate);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.et_date_activityUpdate: {

                DialogFragment datePickerDialog = new DatePickerFragment();

                datePickerDialog.show(getFragmentManager(),"Date Picker");
                break;
            }
            //case R.id.tv_operation_activityUpdate :{

            //  break;
            //}
            case R.id.et_type_activityUpdate: {



                break;
            }
            case R.id.et_sum_activityUpdate: {

                break;
            }
            case R.id.et_comment_activityUpdate: {

                break;
            }

            case R.id.btn_save_activityUpdate: {

                ContentValues cv = new ContentValues();

                int newOperation = getOperation();
                String newDate = mEtDate.getText().toString();
                String newType = typeSpinner.getSelectedItem().toString();
                String sum = mEtSum.getText().toString();
                String newComment = mEtComment.getText().toString();

                MainActivity mainActivity = new MainActivity();
                double newSum = mainActivity.getNumbers(sum); // TODO: 15.02.2018 do check  NO string in numb

                cv.put("date", newDate);
                cv.put("operation", newOperation);
                cv.put("type", newType);
                cv.put("sum", newSum);
                cv.put("comment", newComment);

                getContentResolver().update(CONTENT_URI_MONEY, cv, "_id = " + "'" + mCardID + "'", null);

                onBackPressed();
                break;
            }
            case R.id.btn_cancel_activityUpdate: {
                onBackPressed();
                break;
            }

            default:

                break;

        }

    }

    private int getOperation() {

        int opertion = 0;
        String newOperation = operationSpinner.getSelectedItem().toString();

        if (newOperation.equals(spinnerList.get(0))) {
            return 1;
        } else {
            return -1;
        }

    }
}
