package com.example.workuser.moneycontrolcenter.dialog;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.workuser.moneycontrolcenter.R;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private int year;
    private int month;
    private int day;
    private String currentDate;
    private EditText date;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        date = getActivity().findViewById(R.id.et_date_activityUpdate);
        currentDate = date.getText().toString();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
        Date date1 = format.parse(currentDate, new ParsePosition(0));

        c.setTime(date1);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Date newDate = new Date(year,month,dayOfMonth);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy");
        String newDateStr = formatter.format(newDate);

        date.setText(newDateStr);

    }
}
