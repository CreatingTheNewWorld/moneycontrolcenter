package com.example.workuser.moneycontrolcenter.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.workuser.moneycontrolcenter.MainActivity;
import com.example.workuser.moneycontrolcenter.R;
import com.example.workuser.moneycontrolcenter.adapter.JournalAdapter;
import com.example.workuser.moneycontrolcenter.model.EntryModel;

import java.util.ArrayList;
import java.util.List;

import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;

public class JournalActivity extends AppCompatActivity implements View.OnClickListener, JournalAdapter.ItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private ImageButton mImageButtonAddIncome;
    private ImageButton mImageButtonJournal;
    private ImageButton mImageButtonBalance;
    private ImageButton mImageButtonMoneyBox;
    private ImageButton mImageButtonMoneyGraph;

    private RecyclerView mRecyclerView;

    private JournalAdapter mJournalAdapter;
    private List<EntryModel> mEntryModelLIst;

    private final int DELETE = 101;
    private final int UPDATE = 201;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setImagesView();

        setListeners();

        mRecyclerView = findViewById(R.id.rv_journalActivity);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setRecyclerView();

        registerForContextMenu(mRecyclerView);

        getLoaderManager().restartLoader(1,null,this);


    }

    private void setRecyclerView() {

        mJournalAdapter = new JournalAdapter(getList(),this,this);
        mRecyclerView.setAdapter(mJournalAdapter);

    }

    private List<EntryModel> getList() {

        mEntryModelLIst = new ArrayList<>();

        String sign = null;

        Cursor cursor = getContentResolver().query(CONTENT_URI_MONEY,null,
                null,null,null);

        if(cursor!= null && cursor.moveToLast()){

            do {

                String comment = cursor.getString(cursor.getColumnIndex("comment"));
                String date = cursor.getString(cursor.getColumnIndex("date"));
                int operation = cursor.getInt(cursor.getColumnIndex("operation"));
                int ID = cursor.getInt(cursor.getColumnIndex("_id"));
                String type = cursor.getString(cursor.getColumnIndex("type"));
                double sum = cursor.getDouble(cursor.getColumnIndex("sum"));

                EntryModel entryModel = new EntryModel(date,operation,sum,comment,type,ID);

                mEntryModelLIst.add(entryModel);
            }while(cursor.moveToPrevious());

            cursor.close();
        }
        return mEntryModelLIst;
    }

    private void setListeners() {

        mImageButtonAddIncome.setOnClickListener(this);
        mImageButtonJournal.setOnClickListener(this);
        mImageButtonBalance.setOnClickListener(this);
        mImageButtonMoneyBox.setOnClickListener(this);
        mImageButtonMoneyGraph.setOnClickListener(this);

    }

    private void setImagesView() {
        mImageButtonAddIncome = findViewById(R.id.iB_addIncome_activity_main);
        mImageButtonJournal = findViewById(R.id.iB_journal_activity_main);
        mImageButtonBalance = findViewById(R.id.iB_balance_activity_main);
        mImageButtonMoneyBox = findViewById(R.id.iB_moneyBox_activity_main);
        mImageButtonMoneyGraph = findViewById(R.id.iB_graph_activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // TODO: 16.02.2018 do with notifyChange
        setRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        //AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //long itemID = menuInfo.id;

        int position = JournalAdapter.mPosition;
        int ID = mEntryModelLIst.get(position).getID();

        switch (item.getItemId()){

            case DELETE:{

                getContentResolver().delete(CONTENT_URI_MONEY,"_id = " + "'" + ID + "'", null);
                // TODO: 14.02.2018
                //for update RecyclerView invoke method setRecyclerView()
                setRecyclerView();

                break;
            }
            case UPDATE:{

                Intent startUpdateActivity  = new Intent(this,UpdateActivity.class);

                startUpdateActivity.putExtra("cardId",ID);
                startActivity(startUpdateActivity);

                break;
            }
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Intent startActivityIntent = null;

        switch (v.getId()){

            case R.id.iB_addIncome_activity_main : {
                startActivityIntent = new Intent(this,MainActivity.class);
                break;
            }
            case R.id.iB_journal_activity_main : {
                break;
            }
            case R.id.iB_balance_activity_main : {
                startActivityIntent = new Intent(this,BalanceActivity.class);
                break;
            }case R.id.iB_moneyBox_activity_main : {
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.iB_graph_activity_main: {
                startActivityIntent = new Intent(this,GraphActivity.class);
                break;
            }

            default:

                break;

        }
        if(startActivityIntent != null) {
            startActivity(startActivityIntent);
        }

    }

    @Override
    public void onItemClick(int position) {

        int ID = mEntryModelLIst.get(position).getID();

        Intent startUpdateActivity = new Intent(this,UpdateActivity.class);

        startUpdateActivity.putExtra("cardId",ID);
        startActivity(startUpdateActivity);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,CONTENT_URI_MONEY,null,null,
                null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
