package com.example.workuser.moneycontrolcenter.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.DragAndDropPermissions;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.workuser.moneycontrolcenter.R;
import com.example.workuser.moneycontrolcenter.model.EntryModel;

import java.util.List;

import static com.example.workuser.moneycontrolcenter.MainActivity.INCOME;


public class JournalAdapter extends RecyclerView.Adapter<JournalAdapter.ViewHolder> {


    private List<EntryModel> mEntryModelList;
    private ItemClickListener mItemClickListener;
    private Context mContext;

    private final int DELETE = 101;
    private final int UPDATE = 201;

    public static int mPosition = 0;

    //cлушатель для адаптера
    public interface ItemClickListener{
        void onItemClick(int position);
    }

    public JournalAdapter(List<EntryModel> mEntryModels, ItemClickListener mItemClickListener, Context context) {
        this.mEntryModelList = mEntryModels;
        this.mItemClickListener = mItemClickListener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_journal_list,parent,false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_journal_card,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

//в методе наполняем наши данные
        holder.mTvJournalComment.setText(String.valueOf(mEntryModelList.get(position).getComment()));
        //holder.mTvJournalCurrentOperation.setText(String.valueOf(mEntryModelList.get(position).getCurrentOperation()));
        holder.mTvJournalDate.setText(String.valueOf(mEntryModelList.get(position).getDate()));
        holder.mTvJournalSum.setText(String.valueOf(mEntryModelList.get(position).getSum()));
        holder.mTvJournalType.setText(String.valueOf(mEntryModelList.get(position).getType()));

        if (mEntryModelList.get(position).getCurrentOperation() == INCOME){
            holder.mImageView.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.plus));
        }else {
            holder.mImageView.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.minus));
        }


    }

    @Override
    public int getItemCount() {
        return mEntryModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener{

        private TextView mTvJournalComment;
        private TextView mTvJournalCurrentOperation;
        private TextView mTvJournalDate;
        private TextView mTvJournalSum;
        private TextView mTvJournalType;
        private ImageView mImageView;



        public ViewHolder(View itemView) {
            super(itemView);

            mTvJournalComment = (TextView) itemView.findViewById(R.id.tv_itemJournal_comment);
            mTvJournalCurrentOperation = (TextView) itemView.findViewById(R.id.tv_itemJournal_currentOperation);
            mTvJournalDate = (TextView) itemView.findViewById(R.id.tv_itemJournal_date);
            mTvJournalSum = (TextView) itemView.findViewById(R.id.tv_itemJournal_sum);
            mTvJournalType = (TextView) itemView.findViewById(R.id.tv_itemJournal_type);
            mImageView = itemView.findViewById(R.id.imageView_operation);

            //повесили слушатель на mTvJournalSum, а так же можем повесить слушатель на item
            //mTvJournalSum.setOnClickListener(this);
            itemView.setOnClickListener(this);

            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE,DELETE,Menu.NONE,"DELETE");
            menu.add(Menu.NONE,UPDATE,Menu.NONE,"UPDATE");

            mPosition = getLayoutPosition();
        }


        @Override
        public void onClick(View v) {

            int itemPosition = getLayoutPosition();
            mItemClickListener.onItemClick(itemPosition);
        }

    }

}
