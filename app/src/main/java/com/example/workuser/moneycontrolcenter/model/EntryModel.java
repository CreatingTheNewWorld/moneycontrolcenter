package com.example.workuser.moneycontrolcenter.model;

public class EntryModel {

    private String date;
    private int currentOperation;
    private double sum;
    private String comment;
    private String type;
    private int ID;


    public EntryModel() {
    }

    public EntryModel(String date, int currentOperation, double sum, String comment, String type, int ID) {
        this.date = date;
        this.currentOperation = currentOperation;
        this.sum = sum;
        this.comment = comment;
        this.type = type;
        this.ID = ID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCurrentOperation() {
        return currentOperation;
    }

    public void setCurrentOperation(int currentOperation) {
        this.currentOperation = currentOperation;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
