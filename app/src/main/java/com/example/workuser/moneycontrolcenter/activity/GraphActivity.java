package com.example.workuser.moneycontrolcenter.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.workuser.moneycontrolcenter.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;

public class GraphActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageButton mImageButtonAddIncome;
    private ImageButton mImageButtonJournal;
    private ImageButton mImageButtonBalance;
    private ImageButton mImageButtonMoneyBox;
    private ImageButton mImageButtonMoneyGraph;

    private LineChart mLineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        setImagesView();
        setListener();


        mLineChart = findViewById(R.id.lineChart_money);

        ArrayList<ILineDataSet> mILineDataSets = new ArrayList<>();

        LineDataSet lineMoney = new LineDataSet(getEntryMoney(), "Money");
        lineMoney.setColor(Color.RED);

        mILineDataSets.add(lineMoney);

        mLineChart.setData(new LineData(getDate(), mILineDataSets));

    }
    private void setListener() {
        mImageButtonAddIncome.setOnClickListener(this);
        mImageButtonJournal.setOnClickListener(this);
        mImageButtonBalance.setOnClickListener(this);
        mImageButtonMoneyBox.setOnClickListener(this);
        mImageButtonMoneyGraph.setOnClickListener(this);
    }
    private void setImagesView() {
        mImageButtonAddIncome = findViewById(R.id.iB_addIncome_activity_main);
        mImageButtonJournal = findViewById(R.id.iB_journal_activity_main);
        mImageButtonBalance = findViewById(R.id.iB_balance_activity_main);
        mImageButtonMoneyBox = findViewById(R.id.iB_moneyBox_activity_main);
        mImageButtonMoneyGraph = findViewById(R.id.iB_graph_activity_main);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.menu_expense) {

        } else if (id == R.id.menu_income) {

        }
        Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show(); // TODO: 18.02.2018  

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Entry> getEntryMoney() {

        ArrayList<Entry> entryAge = new ArrayList<>();
        for (int i = 0; i < getSum().length; i++) {
            entryAge.add(new Entry(getSum()[i], i));

        }
        return entryAge;
    }

    private List<String> getDate() {

        List<String> resultDate = new ArrayList<>();
        Cursor cursor = getContentResolver().query(CONTENT_URI_MONEY, new String[]{"date"}, "operation = '-1'", null, null);


        if (cursor != null && cursor.moveToFirst()) {

            do {
                String date = cursor.getString(cursor.getColumnIndex("date"));
                resultDate.add(date);
            } while (cursor.moveToNext());

            cursor.close();
        }
        return resultDate;
    }

    private float[] getSum() {

        List<Float> sumList = new ArrayList<>();
        Cursor cursor = getContentResolver().query(CONTENT_URI_MONEY, null, "operation = '-1'", null, null);


        if (cursor != null && cursor.moveToFirst()) {

            do {
                float sum = cursor.getFloat(cursor.getColumnIndex("sum"));
                sumList.add(sum);
            } while (cursor.moveToNext());

            cursor.close();
        }

        float[] result = new float[sumList.size()];

        for (int i = 0; i < sumList.size(); i++) {

            result[i] = sumList.get(i);

        }

        return result;
    }

    @Override
    public void onClick(View v) {

        Intent startActivityIntent = null;

        switch (v.getId()) {

            case R.id.iB_addIncome_activity_main: {
                break;
            }
            case R.id.iB_journal_activity_main: {
                startActivityIntent = new Intent(this, JournalActivity.class);
                break;
            }
            case R.id.iB_balance_activity_main: {
                startActivityIntent = new Intent(this, BalanceActivity.class);
                break;
            }
            case R.id.iB_moneyBox_activity_main: {
                //startActivityIntent = new Intent(this,Main3Activity.class);
                Toast.makeText(this, "button in build progress...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.iB_graph_activity_main: {
                break;
            }
            default:
                break;
        }
        if (startActivityIntent != null) {
            startActivity(startActivityIntent);
        }
    }
}
