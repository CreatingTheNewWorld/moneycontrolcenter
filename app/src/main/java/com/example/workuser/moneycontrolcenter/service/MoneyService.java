package com.example.workuser.moneycontrolcenter.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.example.workuser.moneycontrolcenter.provider.MoneyProvider.CONTENT_URI_MONEY;


public class MoneyService extends IntentService {
    private static final String TAG = "MoneyService";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MoneyService() {
        super("MoneyServiceName");

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        String date = intent.getStringExtra("date");
        Integer operation = intent.getIntExtra("currentOperation",0);
        String type = intent.getStringExtra("type");
        Double sum = intent.getDoubleExtra("sum",0.0);
        String comment = intent.getStringExtra("comment");

        ContentValues cv = new ContentValues();
        cv.put("date", date);
        cv.put("operation", operation);
        cv.put("type", type);
        cv.put("sum", sum);
        cv.put("comment", comment);

        getContentResolver().insert(CONTENT_URI_MONEY, cv);

        Intent broadcastIntent = new Intent("result");
        broadcastIntent.putExtra("type",type);
        broadcastIntent.putExtra("sum",sum);
        broadcastIntent.putExtra("operation",operation);

        sendBroadcast(broadcastIntent);

    }


}
